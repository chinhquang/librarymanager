//
//  SignupResponse.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 5/27/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
class SignupResponse: Mappable {
    var msg :String?
    var code :Int?
    
    required init?(map: Map){
        
    }
    init() {
        
    }

    
    func mapping(map: Map) {
        msg <- map["msg"]
        code <- map["code"]
    }
}
