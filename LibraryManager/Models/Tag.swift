//
//  Tag.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 6/10/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation

import UIKit
import ObjectMapper
class Tag: Mappable {
    
    var name : String?
    var _id : String?
    required init?(map: Map){
        
    }
    init() {
        
    }
    
    
    func mapping(map: Map) {
        
        name <- map["name"]
        _id <- map["_id"]
    }
}
