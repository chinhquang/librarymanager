//
//  MyBooksViewController.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 3/19/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class MyBooksViewController: UIViewController {
    private var id  = "cellid"
    var names = ["Vegetables": ["Tomato", "Potato", "Lettuce"], "Fruits": ["Apple", "Banana"]]
    
    struct Objects {
        var sectionName : String!
        var sectionObjects : [String]!
    }
    var objectArray = [Objects]()
    
    
    let table : UITableView = {
        let tb =  UITableView ()
        return tb
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(table);
        table.translatesAutoresizingMaskIntoConstraints = false
        register()
        setUpLayout()
        setupNavigationBar()
        setTableViewSessionData()
    }
    func register () {
        table.register(BookCell.self, forCellReuseIdentifier: id)
        table.delegate = self
        table.dataSource = self
    }
    private func setTableViewSessionData (){
        for (key, value) in names {
            print("\(key) -> \(value)")
            objectArray.append(Objects(sectionName: key, sectionObjects: value))
        }
    }
    private func setupNavigationBar (){
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "My Books"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editAction(_sender:)))
    }
    @objc func editAction (_sender : UIBarButtonItem){
        
    }
    private func setUpLayout () {
        table.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        table.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        table.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        table.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        table.alwaysBounceVertical = true
        
    }

}
extension MyBooksViewController : UITableViewDelegate{
    
}
extension MyBooksViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return objectArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectArray[section].sectionObjects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id, for: indexPath) as! BookCell
        
        // Configure the cell...
        cell.name = objectArray[indexPath.section].sectionObjects[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return objectArray[section].sectionName
    }
    
    
}

class BookCell : BaseCell{
    var name  : String?{
        didSet{
            labelName.text  = name
        }
    }
    let labelName : UILabel = {
        let lb = UILabel();
        lb.font = UIFont(name: lb.font.fontName, size: 14)
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    override func setupView() {
        super.setupView()
        addSubview(labelName)
        labelName.leadingAnchor.constraint(equalTo: leadingAnchor, constant : 20).isActive = true
        labelName.centerYAnchor.constraint(equalTo: centerYAnchor).isActive =  true
    }
}
