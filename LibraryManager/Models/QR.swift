//
//  QR.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 6/10/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation

import ObjectMapper
class QR: Mappable {
    
    var code : Int?
    var QR : String?
    var msg : String?
    required init?(map: Map){
        
    }
    init() {
        
    }
    
    
    func mapping(map: Map) {
        
        code <- map["code"]
        QR <- map["QR"]
        msg<-map["msg"]
    }
}
