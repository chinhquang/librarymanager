//
//  LibraryTableViewController.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 5/30/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import TagListView
class LibraryViewController: UIViewController {
    let bookcellId = "BookCellId";
    let authorcellId = "AuthorCellId"
    let genrescellId = "GenresCellId"
    var books = [Book](){
        didSet{
            bookCollectionView.reloadData()
        }
    }
    var authors = [Author](){
        didSet{
            authorCollectionView.reloadData()
        }
    }
    
    var tags = [Tag](){
        didSet {
            self.tagListView.removeAllTags()
            for tag in tags
            {
                
                self.tagListView.addTag(tag.name!)
            }
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Library"
        self.navigationController?.setupColor(color:  #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1))
        
       initFromStart()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        setupView()
        fetchData()
        
    }
    func fetchData (){
        
        tagListView.textFont = UIFont(name: "Helvetica", size: view.frame.height * 0.019)!
        tagListView.cornerRadius = view.frame.width * 0.015
        tagListView.paddingX = view.frame.width * 0.038
        tagListView.paddingY = view.frame.width * 0.02 // possible values are .Left, .Center, and .Right
        tagListView.tagBackgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
       
        API().getBookList(){
            result in
            self.books.removeAll()
            for x in result{
                self.books.append(x)
            }
        }
        API().getAuthorList(){
            result in
            self.authors.removeAll()
            for x in result{
                self.authors.append(x)
            }
        }
        API().getTagList(token: API.token){
            result in
            self.tags.removeAll()
            for x in result{
                self.tags.append(x)
            }
        }
    }
    func initFromStart() -> Void {
        bookCollectionView.register(BookCell.self, forCellWithReuseIdentifier: bookcellId);
        bookCollectionView.dataSource = self
        bookCollectionView.delegate = self
        
        authorCollectionView.register(AuthorCell.self, forCellWithReuseIdentifier: authorcellId);
        authorCollectionView.dataSource = self
        authorCollectionView.delegate = self
        
    }

    let  bookLabel : UILabel = {
        let label1 = UILabel()
        label1.text = "Book List"
        label1.translatesAutoresizingMaskIntoConstraints = false
        return label1
    }()
    let bookCollectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout();
        layout.scrollDirection = .horizontal;
        let cv  = UICollectionView(frame: .zero, collectionViewLayout:layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .white
        
        return cv
    }()
    let  authorLabel : UILabel = {
        let label1 = UILabel()
        label1.text = "Author List"
        label1.translatesAutoresizingMaskIntoConstraints = false
        return label1
    }()
    let authorCollectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout();
        layout.scrollDirection = .horizontal;
        let cv  = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .white
        return cv
    }()
    let  genresLabel : UILabel = {
        let label1 = UILabel()
        label1.text = "Genres List"
        
        label1.translatesAutoresizingMaskIntoConstraints = false
        return label1
    }()
    let tagListView : TagListView = {
       
        let taglist = TagListView()
        taglist.alignment = .left
        taglist.translatesAutoresizingMaskIntoConstraints = false
        return taglist
    }()
    func setupView() -> Void {
        
        
        view.addSubview(bookLabel)
        bookLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: view.frame.height * 0.02).isActive = true
        bookLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.width * 0.07).isActive = true
        
        bookLabel.font = UIFont(name: "Helvetica", size: view.frame.height * 0.025)
        
        view.addSubview(bookCollectionView)
        bookCollectionView.topAnchor.constraint(equalTo: bookLabel.bottomAnchor, constant: view.frame.height * 0.01).isActive = true
        bookCollectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: view.frame.width * 0.05).isActive = true
        bookCollectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -view.frame.width * 0.05).isActive = true
        bookCollectionView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.17).isActive = true
        
        view.addSubview(authorLabel)
        authorLabel.topAnchor.constraint(equalTo: bookCollectionView.bottomAnchor, constant: view.frame.height * 0.02).isActive = true
        authorLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.width * 0.07).isActive = true
        
        authorLabel.font = UIFont(name: "Helvetica", size: view.frame.height * 0.025)
        
        view.addSubview(authorCollectionView)
        authorCollectionView.topAnchor.constraint(equalTo: authorLabel.bottomAnchor, constant: view.frame.height * 0.01).isActive = true
        authorCollectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: view.frame.width * 0.05).isActive = true
        authorCollectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -view.frame.width * 0.05).isActive = true
        authorCollectionView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.17).isActive = true
        
        
        view.addSubview(genresLabel)
        genresLabel.topAnchor.constraint(equalTo: authorCollectionView.bottomAnchor, constant: view.frame.height * 0.02).isActive = true
        genresLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.width * 0.07).isActive = true
        
        genresLabel.font = UIFont(name: "Helvetica", size: view.frame.height * 0.025)
        
        view.addSubview(tagListView)
        tagListView.topAnchor.constraint(equalTo: genresLabel.bottomAnchor, constant: view.frame.height * 0.01).isActive = true
        tagListView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: view.frame.width * 0.05).isActive = true
        tagListView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -view.frame.width * 0.05).isActive = true
        tagListView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.17).isActive = true

    }

   

}
extension LibraryViewController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == bookCollectionView{
            BookDetailViewController.BookDetail = books[indexPath.item]
            navigationController?.pushViewController(BookDetailViewController(), animated: true)
        }
    }
}
extension LibraryViewController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if  collectionView == bookCollectionView{
            return books.count
        }
        else if collectionView == authorCollectionView{
            return authors.count
        }
        else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == bookCollectionView{
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: bookcellId, for: indexPath) as? BookCell{
                cell.backgroundColor = .white
                cell.book = books[indexPath.item]
                
                
                return cell
            }
        }
        if collectionView == authorCollectionView{
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: authorcellId, for: indexPath) as? AuthorCell{
                cell.backgroundColor = .white
                cell.author = authors[indexPath.item]
                
                
                return cell
            }
        }
       
        return UICollectionViewCell()
    }
    
    
}
extension LibraryViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width: collectionView.frame.height * 0.6, height: collectionView.frame.height * 0.9);
    }
}
