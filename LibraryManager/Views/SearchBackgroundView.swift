//
//  SearchBackgroundView.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 5/31/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class SearchBackgroundView: UIView {
    
    let imageview : UIImageView = {
       let im = UIImageView(image: #imageLiteral(resourceName: "icons8-search-100"))
        im.contentMode = .scaleAspectFit
        im.translatesAutoresizingMaskIntoConstraints = false
        return im
    }()
    let label : UILabel = {
        let lb = UILabel()
        lb.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        lb.text = "No search results found"
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupView() -> Void {
        addSubview(imageview)
        imageview.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        imageview.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5).isActive = true
        imageview.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        addSubview(label)
        label.topAnchor.constraint(equalTo: imageview.bottomAnchor, constant: 0).isActive = true
        label.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        label.font = UIFont.systemFont(ofSize: 20)
        label.numberOfLines = 0
    }
}
