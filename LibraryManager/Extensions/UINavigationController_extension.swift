//
//  UINavigationController_extension.swift
//  Foodipe
//
//  Created by Chính Trình Quang on 3/28/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit
extension UINavigationController{
    func removeBorder() -> Void {
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
    }
    func setupColor(color : UIColor) -> Void {
        self.navigationBar.barStyle       = UIBarStyle.black // I then set the color using:
        
        self.navigationBar.barTintColor   = color // a lovely red
        
        self.navigationBar.tintColor = UIColor.white // for titles, buttons, etc.
        
    }
    func resetNav() -> Void {
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.backgroundImage(for: .default)
        self.navigationController?.navigationBar.backgroundColor = .none
        self.navigationController?.navigationBar.setBackgroundImage(.none, for: .default)
    }
}
