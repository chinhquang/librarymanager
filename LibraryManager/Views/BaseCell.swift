//
//   BaseItem.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 5/23/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit
class BaseCell : UICollectionViewCell{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("Cannot initiate cell")
    }
    func setupView(){
        backgroundColor = .white
    }
    
}
