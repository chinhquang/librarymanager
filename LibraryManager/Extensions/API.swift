//
//  API.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 5/27/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import Alamofire
class API {
    
    public  var baseURL = "https://hcmus.ericadonis.top/api/"
    public var user = "taikhoan"
    
    func signup(userProfile : UserProfile,completion:  @escaping (SignupResponse) -> Void) {
        let url : String = baseURL + "taikhoan"
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let json = userProfile.convertToJSON()
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        
        
        Alamofire.request(request as URLRequestConvertible)
            .responseJSON { response in
                
                
                print(response.value)
                if let t = response.value as? [String :Any]{
                    print(t["code"])
                    if let d = SignupResponse(JSON: t){
                        completion(d)
                    }
                   
                }
                
        }
    }
    
    func login(username : String? , password : String?,completion:  @escaping (LogInResponse) -> Void) {
        let url : String = baseURL + "login"
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "POST"
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let bodyJSON : NSMutableDictionary = [
        
            "taiKhoan": username ?? "",
            "matKhau": password ?? ""
        ]
        let data = try! JSONSerialization.data(withJSONObject: bodyJSON, options: JSONSerialization.WritingOptions.prettyPrinted)
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
      
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        
        request.allHTTPHeaderFields = ["Content-Type" : "application/json"]
        Alamofire.request(request as URLRequestConvertible)
            .responseJSON { response in
                
                
                print(response.value)
                if let t = response.value as? [String :Any]{
                    print(t["code"])
                    
                    
                    let cac = t[""]
                    if let d = LogInResponse(JSON: t){
                        completion(d)
                    }
                    
                }
                
        }
        
    }
    func borrowBook(book : [String], token : String,completion:  @escaping (QR) -> Void) {
        let url : String = baseURL + "muonsach"
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "POST"
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let current = Date.dateToReturnBook
        let timestamp = current.toTimeStamp()
        
        let bodyJSON : NSMutableDictionary = [
            
            "_chiTiet" : [
                "_sach" : book,
                "ghiChu" : "Trả sách ",
                "ngayHenTra" : timestamp
                
             ]
        ]
        let data = try! JSONSerialization.data(withJSONObject: bodyJSON, options: JSONSerialization.WritingOptions.prettyPrinted)
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        print(token)
        request.allHTTPHeaderFields = ["Content-Type" : "application/json", "authorization" : token]
        Alamofire.request(request as URLRequestConvertible)
            .responseJSON { response in
                
                
                print(response.value)
                if let t = response.value as? [String :Any]{
                    let qr = QR(JSON: t)
                    completion(qr!)
                    
                }
                
        }
    }
    
    func searchByName(bookName : String,completion:  @escaping ([Book]) -> Void){
        let url : String = baseURL + "sach/tensach"
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "POST"
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let bodyJSON : NSMutableDictionary = [
            
            "tenSach": bookName,
            
        ]
        let data = try! JSONSerialization.data(withJSONObject: bodyJSON, options: JSONSerialization.WritingOptions.prettyPrinted)
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        
        request.allHTTPHeaderFields = ["Content-Type" : "application/json"]
        Alamofire.request(request as URLRequestConvertible)
            .responseJSON { response in
                
                
                print(response.value)
                if let list = response.value as? NSArray{
                    var books = [Book]()
                    for x in list{
                        if let x1 = x as? [String:Any]{
                            
                            let bookItem = Book(JSON: x1)
                            
                            books.append(bookItem!)
                        }
                    }
                    completion(books)
                }
                
        }
        
    }
    func bookBorrow(token : String,completion:  @escaping ([Book],[Int] ) -> Void){
        let url : String = baseURL + "muonsach/member"
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "GET"
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        request.allHTTPHeaderFields = ["authorization" : token]
        Alamofire.request(request as URLRequestConvertible)
            .responseJSON { response in
                
                
                print(response.value)
                if let list = response.value as? NSArray{
                    var books = [Book]()
                    var returndate = [Int]()
                    for x in list{
                        
                        
                        if let x1 = x as? [String:Any]{
                            
                            let chitiet = x1["_chiTiet"] as! [String : Any]
                            let d = chitiet["ngayHenTra"] as! Int
                            let sach = chitiet["_sach"] as! NSArray
                            let sach_return  = sach[0] as! [String:Any]
                            books.append(Book(JSON: sach_return)!)
                            returndate.append(d)
                            
                            
                        }
                    }
                    
                    completion(books, returndate)
                }
                
        }
        
    }
    func searchByAuthor(authorname : String,completion:  @escaping ([Book]) -> Void){
        let url : String = baseURL + "sach/tentacgia"
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "POST"
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let bodyJSON : NSMutableDictionary = [
            
            "tenTacGia": authorname,
            
            ]
        let data = try! JSONSerialization.data(withJSONObject: bodyJSON, options: JSONSerialization.WritingOptions.prettyPrinted)
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        
        request.allHTTPHeaderFields = ["Content-Type" : "application/json"]
        Alamofire.request(request as URLRequestConvertible)
            .responseJSON { response in
                
                
                print(response.value)
                if let list = response.value as? NSArray{
                    var books = [Book]()
                    for x in list{
                        if let x1 = x as? [String:Any]{
                            
                            let bookItem = Book(JSON: x1)
                            
                            books.append(bookItem!)
                        }
                    }
                    completion(books)
                }
                
        }
        
    }
    func getUserInfo(token : String,completion:  @escaping (UserProfile) -> Void) {
        let url : String = baseURL + "login"
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "GET"
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
       
        
        request.allHTTPHeaderFields = ["authorization" : token]
        Alamofire.request(request as URLRequestConvertible)
            .responseJSON { response in
                
                
                print(response.value)
                if let t = response.value as? [String :Any]{
                   
                    if let d = UserProfile(JSON: t){
                        completion(d)
                    }
                    
                }
                
        }
        
    }
    func getTagList(token : String,completion:  @escaping ([Tag]) -> Void) {
        let url : String = baseURL + "tag"
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "GET"
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.allHTTPHeaderFields = ["authorization" : token]
        Alamofire.request(request as URLRequestConvertible)
            .responseJSON { response in
                
                
                print(response.value)
                if let list = response.value as? NSArray{
                    var tags = [Tag]()
                    for x in list{
                        if let x1 = x as? [String:Any]{
                            
                            tags.append(Tag(JSON: x1)!)
                            
                        }
                        
                    }
                    completion(tags)
                }
                
        }
        
    }
    func getBookList(completion:  @escaping ([Book]) -> Void) {
        let url : String = baseURL + "sach"
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "GET"
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        Alamofire.request(request as URLRequestConvertible)
            .responseJSON { response in
                
                
                print(response.value)
                if let list = response.value as? NSArray{
                    var books = [Book]()
                    for x in list{
                        if let x1 = x as? [String:Any]{
                            
                            let bookItem = Book(JSON: x1)
                            
                            books.append(bookItem!)
                        }
                    }
                    completion(books)
                }
        }
    }
    func getBookTag(bookID : String, completion:  @escaping ([Tag]) -> Void) {
        let url : String = baseURL + "sach/" + bookID
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "GET"
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        Alamofire.request(request as URLRequestConvertible)
            .responseJSON { response in
                
                
                print(response.value)
                if let t = response.value as? [String:Any]{
                    let tagsJson = t["tag"] as! NSArray
                    var tags = [Tag]()
                    for x in tagsJson{
                        let x1 = x as! [String:Any]
                        tags.append(Tag(JSON: x1)!)
                    }
                    completion(tags)
                }
        }
    }
    func getAuthorList(completion:  @escaping ([Author]) -> Void) {
        let url : String = baseURL + "tacgia"
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        request.httpMethod = "GET"
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        Alamofire.request(request as URLRequestConvertible)
            .responseJSON { response in
                
                
                print(response.value)
                if let list = response.value as? NSArray{
                    var authors = [Author]()
                    for x in list{
                        if let x1 = x as? [String:Any]{
                            let x2 = x1["_id"] as! [String:Any]
                            authors.append(Author(JSON: x2)!)
                            
                        }
                        
                    }
                    completion(authors)
                }
                
        }
        
    }
    func updateUserInfo(id : String,newProfile : UserProfile,token : String,completion:  @escaping (UserProfile) -> Void) {
        let url : String = baseURL + "taikhoan/" + id
        let ulr =  NSURL(string:url as String)
        var request = URLRequest(url: ulr! as URL)
        
        request.httpMethod = "PUT"
        let bodyJSON : NSMutableDictionary = [
            "hoTen": newProfile.hoTen ?? "",
            "ngaySinh": newProfile.ngaySinh ?? "",
            "SDT": newProfile.SDT ?? "",
            "truongDH": newProfile.truongDH ?? "",
            "mssv": newProfile.mssv ?? ""
        ]
        let data = try! JSONSerialization.data(withJSONObject: bodyJSON, options: JSONSerialization.WritingOptions.prettyPrinted)
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        if let json = json {
            print(json)
        }
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue);
        
        request.allHTTPHeaderFields = ["Content-Type" : "application/json","authorization" : token]
        
        Alamofire.request(request as URLRequestConvertible)
            .responseJSON { response in
                
                
                print(response.value)
                if let t = response.value as? [String :Any]{
                    
                    if let d = UserProfile(JSON: t){
                        completion(d)
                    }
                    
                }
                
        }
        
    }
    static var token = ""
    
}

