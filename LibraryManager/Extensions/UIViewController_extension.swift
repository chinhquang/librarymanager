//
//  UIViewController_extension.swift
//  Foodipe
//
//  Created by Chính Trình Quang on 3/29/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController{
   
    func pushview(vc : UIViewController) -> Void {
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func hideNavigationBar() -> Void {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    func showNavigationBar() -> Void {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    func hideTabBar() {
        var frame = self.tabBarController?.tabBar.frame as! CGRect
        frame.origin.y = self.view.frame.size.height + (frame.size.height)
        UIView.animate(withDuration: 0.5, animations: {
            self.tabBarController?.tabBar.frame = frame
        })
    }
    
    func showTabBar() {
        var frame = self.tabBarController?.tabBar.frame as! CGRect
        frame.origin.y = self.view.frame.size.height - (frame.size.height)
        UIView.animate(withDuration: 0.5, animations: {
            self.tabBarController?.tabBar.frame = frame
        })
    }
    
    
}
