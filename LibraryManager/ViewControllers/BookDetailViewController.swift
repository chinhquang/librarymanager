//
//  BookDetailViewController.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 6/5/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import Cosmos
import TagListView
import ChameleonFramework
class BookDetailViewController: UIViewController {
    static var BookDetail : Book?
    let imageView : UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.image = #imageLiteral(resourceName: "placeholder-book-cover-default")
        img.contentMode = .scaleAspectFit
        img.layer.borderColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        img.layer.borderWidth = 1.2
        img.layer.cornerRadius = 5
        return img
        
    }()
    
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    func startIndicator()
    {
        //creating view to background while displaying indicator
        let container: UIView = UIView()
        container.frame = self.view.frame
        container.center = self.view.center
        container.backgroundColor = UIColor.lightGray
        
        //creating view to display lable and indicator
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 118, height: 80)
        loadingView.center = self.view.center
        loadingView.backgroundColor = UIColor.darkGray
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        //Preparing activity indicator to load
        self.activityIndicator = UIActivityIndicatorView()
        self.activityIndicator.frame = CGRect(x: 40, y: 12, width: 40, height: 40)
        self.activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
        loadingView.addSubview(activityIndicator)
        
        //creating label to display message
        let label = UILabel(frame: CGRect(x: 5, y : 55, width :120,height :20))
        label.text = "Uploading..."
        label.textColor = UIColor.white
        label.font = UIFont(name: label.font.fontName, size: 12)
        label.bounds = CGRect(x : 0, y: 0, width : loadingView.frame.size.width / 2, height : loadingView.frame.size.height / 2)
        loadingView.addSubview(label)
        container.addSubview(loadingView)
        self.view.addSubview(container)
        
        self.activityIndicator.startAnimating()
    }
    func stopIndicator()
    {
        UIApplication.shared.endIgnoringInteractionEvents()
        self.activityIndicator.stopAnimating()
        ((self.activityIndicator.superview as UIView!).superview as UIView!).removeFromSuperview()
    }
    let borrowButton : UIButton = {
        let button  = UIButton(type: .system)
        button.setTitle("Borrow", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .white
        button.backgroundColor = .cyan
        button.addTarget(self, action: #selector(borrow), for: .touchUpInside)
        
        return  button
    }()
    @objc func borrow (){
        let t = BookDetailViewController.BookDetail?.name
        API().borrowBook(book: [t!], token: API.token){
            res  in
            if res.code == 0{
                let alert = UIAlertController(title: "Error? ", message: res.msg, preferredStyle: .actionSheet)
                
                
                
                alert.addAction(UIAlertAction(title: "OK", style: .default) { [unowned self]  result in
                    
                    
                    
                })
                
                self.present(alert, animated: true, completion: nil)
            }else {
                print(res.QR)
                let alert = UIAlertController(title: "Do you want to borrow this book? ", message: "You must return after 5 days from now", preferredStyle: .alert)
                
                
                
                alert.addAction(UIAlertAction(title: "OK", style: .default) { [unowned self]  result in
                    QRViewController.url = res.QR!
                    self.navigationController?.pushViewController(QRViewController(), animated: true)
                    
                    
                })
                alert.addAction(UIAlertAction(title: "Cancel", style: .destructive) { [unowned self]  result in
                    
                    
                    
                })
                self.present(alert, animated: true, completion: nil)
            }
            
        }
    }
    let tagLabel : UILabel = {
        let lb  = UILabel()
        lb.text = "Genres"
        lb.translatesAutoresizingMaskIntoConstraints = false
       return lb
    }()
    let nameLabel : UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.numberOfLines = 0
        lb.textColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        return lb
    }()
    let authorLabel : UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.numberOfLines = 0
        lb.textColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        return lb
    }()
    let releaseDateLabel : UILabel = {
        let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.numberOfLines = 0
        return lb
    }()
    let rating : CosmosView = {
        let rt = CosmosView()
        rt.translatesAutoresizingMaskIntoConstraints = false
        return rt
    }()
    let tagListView : TagListView = {
        
        let taglist = TagListView()
        taglist.alignment = .left
        taglist.translatesAutoresizingMaskIntoConstraints = false
        return taglist
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = .white
        
        self.navigationController?.resetNav()
        navigationItem.title = "Book"
        self.navigationController?.setupColor(color: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1))
        
        setupView()
        setupData()
    }
    
    func setupView() -> Void {
        self.view.addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: view.frame.height * 0.025).isActive = true
        imageView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: view.frame.width * 0.06).isActive = true
        imageView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.29).isActive = true
        imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1.5).isActive = true
        self.view.addSubview(nameLabel)
        
        nameLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: view.frame.height * 0.03).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: view.frame.width * 0.06).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -view.frame.width * 0.04).isActive = true
        
        
        nameLabel.font = UIFont(name: "Helvetica", size: view.frame.height * 0.022)
        
        self.view.addSubview(authorLabel)
        
        authorLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 6).isActive = true
        authorLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: view.frame.width * 0.06).isActive = true
        authorLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -view.frame.width * 0.04).isActive = true
        
        
        authorLabel.font = UIFont(name: "Helvetica", size: view.frame.height * 0.02)
        
        self.view.addSubview(releaseDateLabel)
        
        releaseDateLabel.topAnchor.constraint(equalTo: authorLabel.bottomAnchor, constant: 6).isActive = true
        releaseDateLabel.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: view.frame.width * 0.06).isActive = true
        releaseDateLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -view.frame.width * 0.04).isActive = true
        
        
        releaseDateLabel.font = UIFont(name: "Helvetica", size: view.frame.height * 0.02)
        self.view.addSubview(rating)
        
        rating.topAnchor.constraint(equalTo: releaseDateLabel.bottomAnchor, constant: 12).isActive = true
        rating.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: view.frame.width * 0.06).isActive = true
        rating.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -view.frame.width * 0.04).isActive = true
        rating.heightAnchor.constraint(equalTo: rating.widthAnchor, multiplier: 0.2)
        rating.rating = 3.5
        rating.settings.starSize = Double(rating.frame.height - 3)
        rating.settings.starMargin = 0
        rating.text = "Rating"
        
        self.view.addSubview(borrowButton)
        borrowButton.topAnchor.constraint(equalTo: imageView.bottomAnchor,constant: view.frame.height * 0.01).isActive = true
        borrowButton.centerXAnchor.constraint(equalTo: imageView.centerXAnchor).isActive = true
        borrowButton.widthAnchor.constraint(equalTo: imageView.widthAnchor,multiplier: 1,constant: 0).isActive = true
        borrowButton.heightAnchor.constraint(equalTo: borrowButton.widthAnchor,multiplier: 0.3,constant: 0).isActive = true
        view.layoutIfNeeded()
        borrowButton.layer.cornerRadius = borrowButton.frame.height/2
        borrowButton.titleLabel?.font = UIFont.systemFont(ofSize: borrowButton.frame.height*0.5)
      
        borrowButton.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        
        let divider = UIView()
        divider.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(divider)
        
        divider.heightAnchor.constraint(equalToConstant: 1).isActive = true
        divider.topAnchor.constraint(equalTo: borrowButton.bottomAnchor, constant: 10).isActive = true
        
        divider.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        
        divider.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        divider.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
        
        view.addSubview(tagLabel)
        tagLabel.topAnchor.constraint(equalTo: divider.bottomAnchor, constant: 15).isActive = true
        tagLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.frame.width * 0.06).isActive = true
        
        tagLabel.font = UIFont(name: "Helvetica", size: view.frame.height * 0.02)
        
        view.addSubview(tagListView)
        tagListView.topAnchor.constraint(equalTo: tagLabel.bottomAnchor, constant: 10).isActive = true
        tagListView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: view.frame.width * 0.05).isActive = true
        tagListView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -view.frame.width * 0.05).isActive = true
        tagListView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        tagListView.textFont = UIFont(name: "Helvetica", size: view.frame.height * 0.019)!
        tagListView.cornerRadius = view.frame.width * 0.015
        tagListView.paddingX = view.frame.width * 0.038
        tagListView.paddingY = view.frame.width * 0.02 // possible values are .Left, .Center, and .Right
        tagListView.tagBackgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
    }
    func setupData() -> Void {
        if let detail = BookDetailViewController.BookDetail {
            nameLabel.text = detail.name
            authorLabel.text = detail.author
            let timestamp = detail.releaseDate!/1000
            releaseDateLabel.text = "Released date : "  + timestamp.toDateTime().toString()
            imageView.sd_setImage(with: URL(string: detail.image!), placeholderImage: #imageLiteral(resourceName: "placeholder-book-cover-default"))
            
            let bookid = detail.id
            API().getBookTag(bookID: bookid!){
                result in
                for x in result
                {
                    self.tagListView.addTag(x.name!)
                }
                
            }
            
        }
    }


}
