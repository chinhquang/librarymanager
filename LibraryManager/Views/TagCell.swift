//
//  TagCell.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 6/10/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class TagCell: BaseCell {
    let tagLabel : UILabel = {
        let lb  = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    override func setupView() {
        addSubview(tagLabel)
        tagLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        
        tagLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        
        tagLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8).isActive = true
        tagLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8).isActive = true
        tagLabel.widthAnchor.constraint(lessThanOrEqualToConstant: UIScreen.main.bounds.width - 8 * 2 - 8 * 2).isActive = true
        tagLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 20).isActive = true
        self.backgroundColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        self.tagLabel.textColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 1)
        self.layer.cornerRadius = 4
        
    }
    
    
}
