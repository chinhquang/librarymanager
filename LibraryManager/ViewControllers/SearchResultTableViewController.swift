//
//  SearchResultTableViewController.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 5/30/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class SearchResultTableViewController: UITableViewController, UISearchControllerDelegate, UISearchBarDelegate {
        let searchController = UISearchController(searchResultsController: nil)
    var testData = ["1","2","3","5"]
    var result = [Book](){
        didSet{
            tableView.reloadData()
            if self.result.count > 0{
                tableView.backgroundView = .none
                
            }else{
                tableView.backgroundView = SearchBackgroundView()
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
        navigationItem.title = "Search"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Books"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.searchBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        // Setup the Scope Bar
        searchController.searchBar.scopeButtonTitles = ["By name", "By author"]
        searchController.searchBar.delegate = self        // Uncomment the following line to preserve selection between presentations
        UI.addDoneButton(controls: [searchController.searchBar])
        self.navigationController?.setupColor(color: #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        tableView.tableFooterView = UIView()
        tableView.backgroundView = SearchBackgroundView()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        result.removeAll()
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return result.count
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        BookDetailViewController.BookDetail = result[indexPath.row]
        navigationController?.pushViewController(BookDetailViewController(), animated: true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count != 0{
            if searchController.searchBar.selectedScopeButtonIndex == 0 {
                print("Search by book name")
                API().searchByName(bookName: searchText){
                    res in
                    self.result.removeAll()
                    for x in res{
                        self.result.append(x)
                    }
                }
            }
            else{
                print("Search by author name")
                API().searchByAuthor(authorname: searchText){
                    res in
                    self.result.removeAll()
                    for x in res{
                        self.result.append(x)
                    }
                }
            }
            
        }else {
            result.removeAll()
            
        }
        
    }
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        if searchController.searchBar.selectedScopeButtonIndex == 1{
            
            if searchBar.text!.count != 0{
                if searchBar.selectedScopeButtonIndex == 0 {
                    print("Search by book name")
                    API().searchByName(bookName: searchBar.text!){
                        res in
                        self.result.removeAll()
                        for x in res{
                            self.result.append(x)
                        }
                    }
                }
                else{
                    print("Search by author name")
                    API().searchByAuthor(authorname: searchBar.text!){
                        res in
                        self.result.removeAll()
                        for x in res{
                            self.result.append(x)
                        }
                    }
                }
                
            }else {
                result.removeAll()
                
            }
        }else {
            if searchBar.text!.count != 0{
                if searchBar.selectedScopeButtonIndex == 0 {
                    print("Search by book name")
                    API().searchByName(bookName: searchBar.text!){
                        res in
                        self.result.removeAll()
                        for x in res{
                            self.result.append(x)
                        }
                    }
                }
                else{
                    print("Search by author name")
                    API().searchByAuthor(authorname: searchBar.text!){
                        res in
                        self.result.removeAll()
                        for x in res{
                            self.result.append(x)
                        }
                    }
                }
                
            }else {
                result.removeAll()
                
            }
        }

    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        cell.textLabel?.text = result[indexPath.row].name! + " - " + result[indexPath.row].author!
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
