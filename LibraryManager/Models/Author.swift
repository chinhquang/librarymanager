//
//  Author.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 6/2/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation

import UIKit
import ObjectMapper
class Author: Mappable {
    
    var name : String?
    var img : String?
    required init?(map: Map){
        
    }
    init() {
        
    }
    init(name : String, url : String){
        self.name = name
        self.img = url
    }
    
    func mapping(map: Map) {
        
        name <- map["tenTacGia"]
    }
}
