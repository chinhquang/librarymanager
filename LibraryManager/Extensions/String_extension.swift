//
//  UIString_extension.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 5/28/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
extension String{
    func toDate() -> Date? {
       
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let date = dateFormatter.date(from: self)
        return date
    }
}
