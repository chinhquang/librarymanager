//
//  Date_extension.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 5/28/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
extension Date{
    func toTimeStamp() -> Int {
        return Int(self.timeIntervalSince1970)
    }
    func toString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let dateString = dateFormatter.string(from: self)
        return dateString
    }
    
    static var yesterday: Date { return Date().dayBefore }
    static var dateToReturnBook:  Date { return Date().dateToReturn }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dateToReturn: Date {
        return Calendar.current.date(byAdding: .day, value: 5, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dateToReturn.month != month
    }
}
