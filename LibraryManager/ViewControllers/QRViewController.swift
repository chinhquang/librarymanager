//
//  QRViewController.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 6/11/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import UIKit

class QRViewController: UIViewController,UIGestureRecognizerDelegate {
    
    
    static var url  = String()
   
   
    var ImageView: UIImageView = {
        let im = UIImageView()
        im.contentMode = .scaleAspectFit
        im.translatesAutoresizingMaskIntoConstraints = false
        return im
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(ImageView)
        ImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        ImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        ImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        ImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
    }
    func setRightButton() -> Void {
        let rightBtn = UIButton(type: .system)
        rightBtn.addTarget(self, action: #selector(addActivityController), for: .touchUpInside)
        rightBtn.setImage(#imageLiteral(resourceName: "upload-symbol").withRenderingMode(.automatic), for: .normal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBtn)
        
    }
    @objc func addActivityController() -> Void {
        let itemshare : UIImage = ImageView.image!
        let activityVC = UIActivityViewController(activityItems: [itemshare], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        activityVC.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop,UIActivity.ActivityType.postToFacebook,UIActivity.ActivityType.assignToContact,UIActivity.ActivityType.mail,UIActivity.ActivityType.message ]
        self.present(activityVC, animated: true, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = .white
        setRightButton()
        print(QRViewController.url)
        ImageView.setImg(url: QRViewController.url)
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
