//
//  Book.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 6/2/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation

import UIKit
import ObjectMapper
class Book: Mappable {
    var id : String?
    var name : String?
    var author : String?
    var image : String?
    var releaseDate : Int?
    var state : Int?
    var seri : String?
  
    required init?(map: Map){
        
    }
    init() {
        
    }
    init(id  : String,name : String,author : String,image : String, releaseDate : Int, state: Int, seri : String){
        self.name = name
        self.author = author
        self.image = image
        self.id  = id
        self.releaseDate = releaseDate
        self.seri = seri
        self.state = state
    }
    
    func mapping(map: Map) {
        
        name <- map["tenSach"]
        author <- map ["tenTacGia"]
        id <- map["_id"]
        image <- map["url"]
        releaseDate <- map["ngayXuatBan"]
        state <- map ["trangThai"]
        seri <- map ["seri"]
        
    }
}
