//
//  BookCellList.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 6/2/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import SDWebImage
import UIKit
class AuthorCellList: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let cellId = "AuthorCellId"; // same as above unique id
    var authors = [Author](){
        didSet{
            collectionView.reloadData()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame);
        
        setupViews();
        collectionView.register(AuthorCell.self, forCellWithReuseIdentifier: cellId); //register custom UICollectionViewCell class.
        // Here I am not using any custom class
        API().getAuthorList(){
            result in
            self.authors.removeAll()
            for x in result{
                self.authors.append(x)
            }
            
        }
    }
    
    
    func setupViews(){
        
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        addSubview(collectionView);
        
        collectionView.delegate = self;
        collectionView.dataSource = self;
        
        collectionView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true;
        collectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true;
        collectionView.topAnchor.constraint(equalTo: label.bottomAnchor).isActive = true;
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true;
        
        label.font = UIFont(name: "HelveticaNeue-Bold", size: self.frame.width * 0.045)
        
    }
    let label : UILabel={
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout();
        layout.scrollDirection = .horizontal; //set scroll direction to horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout);
        cv.backgroundColor = .white; //testing
        cv.translatesAutoresizingMaskIntoConstraints = false;
        return cv;
    }();
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? AuthorCell{
            cell.backgroundColor = .white
            cell.author = authors[indexPath.item]
            
            
            return cell
        }
        
        return UICollectionViewCell();
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  authors.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.height * 0.4, height: self.frame.height * 0.7);
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class AuthorCell : BaseCell
{
    //http://image.phimmoi.net/film/1870/poster.medium.jpg
    let size  = UIScreen.main.bounds
//    var book : Book?{
//        didSet{
//
//            self.imageView.sd_setImage(with: URL(string: book!.image!), placeholderImage: #imageLiteral(resourceName: "placeholder-book-cover-default"))
//            self.authorLabel.text = self.book?.author
//            self.nameLabel.text = self.book?.name
//
//        }
//    }
    var author : Author?{
        didSet{
            
            self.imageView.sd_setImage(with: URL(string: self.author!.img ?? ""), placeholderImage: #imageLiteral(resourceName: "author-placeholder"))
            self.authorLabel.text = self.author?.name
        }
    }
    let imageView : UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleAspectFill
       
        img.layer.masksToBounds = true
        return img
    }()

    let authorLabel : UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    override func setupView() {
        super.setupView()
        addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: widthAnchor).isActive = true
        
        
        addSubview(authorLabel)
        authorLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor).isActive = true
        authorLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        authorLabel.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        authorLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        layoutIfNeeded()
        let  fontsize = size.width / 35
        authorLabel.textAlignment = .center
        authorLabel.font = UIFont.systemFont(ofSize: fontsize)
        authorLabel.textColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        imageView.layer.cornerRadius = imageView.frame.width/2
        imageView.layer.masksToBounds = true
    }
}
