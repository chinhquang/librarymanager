//
//  BookCellList.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 6/2/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import SDWebImage
import UIKit
protocol BookCellListDelegate: class {
    func bookList(didSelectAt indexPath : IndexPath)
   
}
class BookCellList: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    weak var delegate : BookCellListDelegate?
    let cellId = "BookCellId"; // same as above unique id
    var books = [Book](){
        didSet{
            collectionView.reloadData()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame);
        
        setupViews();
        collectionView.register(BookCell.self, forCellWithReuseIdentifier: cellId); //register custom UICollectionViewCell class.
        // Here I am not using any custom class
        
        API().getBookList(){
            result in
            self.books.removeAll()
            for x in result{
                self.books.append(x)
            }
        }
//        //test
//        books.append(Book(id: "", name: "Harry", author: "Conan", image: "", releaseDate: 1999, state: 1, seri: "1"))
//        books.append(Book(id: "", name: "Harry", author: "Conan", image: "", releaseDate: 1999, state: 1, seri: "1"))
//        books.append(Book(id: "", name: "Harry", author: "Conan", image: "", releaseDate: 1999, state: 1, seri: "1"))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.bookList(didSelectAt: indexPath)
    }
    func setupViews(){
        
        addSubview(label)
        label.anchor(top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: nil)
        addSubview(collectionView);
        
        collectionView.delegate = self;
        collectionView.dataSource = self;
        
        collectionView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true;
        collectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true;
        collectionView.topAnchor.constraint(equalTo: label.bottomAnchor).isActive = true;
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true;
        
        label.font = UIFont(name: "HelveticaNeue-Bold", size: self.frame.width * 0.045)
        
    }
    let label : UILabel={
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout();
        layout.scrollDirection = .horizontal; //set scroll direction to horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout);
        cv.backgroundColor = .white; //testing
        cv.translatesAutoresizingMaskIntoConstraints = false;
        return cv;
    }();
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? BookCell{
            cell.backgroundColor = .white
            cell.book = books[indexPath.item]
            
            
            return cell
        }
        
        return UICollectionViewCell();
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  books.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.height * 0.4, height: self.frame.height * 0.7);
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
class BookCell : BaseCell
{
    //http://image.phimmoi.net/film/1870/poster.medium.jpg
    let size  = UIScreen.main.bounds
    var book : Book?{
        didSet{
            
            self.imageView.sd_setImage(with: URL(string: book!.image!), placeholderImage: #imageLiteral(resourceName: "placeholder-book-cover-default"))
            self.authorLabel.text = self.book?.author
            self.nameLabel.text = self.book?.name
            
        }
    }
    let imageView : UIImageView = {
        let img = UIImageView(frame: .zero)
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleAspectFit
        img.layer.cornerRadius = 4
        img.layer.masksToBounds = true
        return img
    }()
    let nameLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let authorLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    override func setupView() {
        super.setupView()
        addSubview(imageView)
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        imageView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.8).isActive = true
        addSubview(nameLabel)
        nameLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor).isActive = true
        nameLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        nameLabel.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        
        addSubview(authorLabel)
        authorLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor).isActive = true
        authorLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        authorLabel.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        layoutIfNeeded()
        let  fontsize = size.width / 35
        nameLabel.font = UIFont.systemFont(ofSize: fontsize + 2 )
        authorLabel.font = UIFont.systemFont(ofSize: fontsize - 1)
        authorLabel.textColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
    }
}
