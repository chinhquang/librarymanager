//
//  Double_extensions.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 6/6/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
extension Int {
    func toDateTime() -> Date {
        return Date(timeIntervalSince1970: TimeInterval(self))
    }
}
