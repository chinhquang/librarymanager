//
//  UserProfile.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 5/27/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation
import ObjectMapper
class UserProfile: Mappable {
    var id : String?
    var hoTen: String?
    var ngaySinh: Int?
    var taiKhoan: String?
    var matKhau: String?
    var SDT: String?
    var truongDH: String?
    var mssv: String?
    required init?(map: Map){
        
    }
    init() {
        
    }
    
    
    func mapping(map: Map) {
        id <- map["_id"]
        hoTen <- map["hoTen"]
        ngaySinh <- map["ngaySinh"]
        taiKhoan <- map["taiKhoan"]
        matKhau <- map["matKhau"]
        SDT <- map["SDT"]
        truongDH <- map["truongDH"]
        mssv <- map["mssv"]
    }
}
extension UserProfile{
    func convertToJSON() -> NSString? {
        let bodyJSON : NSMutableDictionary = [
            "hoTen": self.hoTen ?? "",
            "ngaySinh": self.ngaySinh ?? "",
            "taiKhoan": self.taiKhoan ?? "",
            "matKhau": self.matKhau ?? "",
            "SDT": self.SDT ?? "",
            "truongDH": self.truongDH ?? "",
            "mssv": self.mssv ?? ""
        ]
        let data = try! JSONSerialization.data(withJSONObject: bodyJSON, options: JSONSerialization.WritingOptions.prettyPrinted)
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        return json
    }
}
