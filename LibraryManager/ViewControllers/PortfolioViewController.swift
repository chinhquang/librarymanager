//
//  PortfolioViewController.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 6/5/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation


import UIKit

class PortfolioViewController: UIViewController , UITextFieldDelegate{
    
    var activeField: UITextField?
    var lastOffset: CGPoint?
    var keyboardHeight: CGFloat!
    var heightContentViewContraint = NSLayoutConstraint()
    let scrollView : UIScrollView = {
        let sv = UIScrollView()
        sv.backgroundColor = .white
        
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    let BackButton : UIButton = {
        let button  = UIButton(type: .system)
        button.setTitle("BACK", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .white
        button.backgroundColor = .cyan
        
        return  button
    }()
    let portfolioLabel : UILabel = {
        let label = UILabel()
        label.text = "MY PORTFOLIO"
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    let messageLabel : UILabel = {
        let label = UILabel()
        label.text = "All of your information will be here"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let sdtTextField : TextField = {
        let textfield  = TextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false
       
        textfield.unenableEditting()
        return textfield
    }()
   
    let fullNameTextField : TextField = {
        let textfield  = TextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false
      
        textfield.unenableEditting()
        return textfield
    }()
    
    let collegeTextField : TextField = {
        let textfield  = TextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false

        textfield.unenableEditting()
        return textfield
    }()
    let dateOfBirthTextField : TextField = {
        let textfield  = TextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false

        textfield.unenableEditting()
        return textfield
    }()
    let studentCodeTextField : TextField = {
        let textfield  = TextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false

        textfield.unenableEditting()
        return textfield
    }()
    let usernameTextField : TextField = {
        let textfield  = TextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false

        textfield.unenableEditting()
        return textfield
    }()
    
    func setupBackBarButton(){
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "circled-left--v1").withRenderingMode(.alwaysOriginal), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.addTarget(self, action: #selector(backToPrevious), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(scrollView)
        
        setupLayout()
        setupUI()
        setupBackBarButton()
        navigationItem.title = "Portfolio"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = .white
        API().getUserInfo(token: API.token){
            user in
            self.fullNameTextField.text = user.hoTen
            self.sdtTextField.text = user.SDT
            self.collegeTextField.text = user.truongDH
            self.studentCodeTextField.text = user.mssv
            self.usernameTextField.text = user.taiKhoan
            let date_of_birth = user.ngaySinh?.toDateTime().toString()
            self.dateOfBirthTextField.text = date_of_birth
            
        }
    }
    fileprivate func setupLayout(){
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        scrollView.addSubview(contentView)
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        contentView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        heightContentViewContraint = contentView.heightAnchor.constraint(equalToConstant: view.frame.height*1.05)
        heightContentViewContraint.isActive = true
        
        
        contentView.addSubview(portfolioLabel)
        portfolioLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        portfolioLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: view.frame.height * 0.17).isActive = true
        portfolioLabel.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.05).isActive = true
        
        contentView.addSubview(messageLabel)
        messageLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        messageLabel.topAnchor.constraint(equalTo: portfolioLabel.bottomAnchor, constant: view.frame.height * 0.012).isActive = true
        messageLabel.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.018).isActive = true
        
        contentView.addSubview(sdtTextField)
        sdtTextField.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: view.frame.height * 0.18).isActive = true
        sdtTextField.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        sdtTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.91).isActive = true
        sdtTextField.heightAnchor.constraint(equalTo: sdtTextField.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        
        contentView.addSubview(fullNameTextField)
        fullNameTextField.bottomAnchor.constraint(equalTo: sdtTextField.topAnchor, constant: -view.frame.height * 0.03).isActive = true
        fullNameTextField.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        fullNameTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.91).isActive = true
        fullNameTextField.heightAnchor.constraint(equalTo: fullNameTextField.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        
        
        
        
        contentView.addSubview(usernameTextField)
        usernameTextField.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        usernameTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.91).isActive = true
        usernameTextField.heightAnchor.constraint(equalTo: usernameTextField.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        usernameTextField.topAnchor.constraint(equalTo:  sdtTextField.bottomAnchor, constant: view.frame.height * 0.03).isActive = true
        
        contentView.addSubview(studentCodeTextField)
        studentCodeTextField.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        studentCodeTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.91).isActive = true
        studentCodeTextField.heightAnchor.constraint(equalTo: fullNameTextField.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        studentCodeTextField.topAnchor.constraint(equalTo: usernameTextField.bottomAnchor, constant: view.frame.height * 0.03).isActive = true
        
        contentView.addSubview(dateOfBirthTextField)
        dateOfBirthTextField.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        dateOfBirthTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.91).isActive = true
        dateOfBirthTextField.heightAnchor.constraint(equalTo: fullNameTextField.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        dateOfBirthTextField.topAnchor.constraint(equalTo: studentCodeTextField.bottomAnchor, constant: view.frame.height * 0.03).isActive = true
        
        contentView.addSubview(collegeTextField)
        collegeTextField.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        collegeTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.91).isActive = true
        collegeTextField.heightAnchor.constraint(equalTo: fullNameTextField.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        collegeTextField.topAnchor.constraint(equalTo: dateOfBirthTextField.bottomAnchor, constant: view.frame.height * 0.03).isActive = true
        
        
        contentView.addSubview(BackButton)
        BackButton.topAnchor.constraint(equalTo: collegeTextField.bottomAnchor,constant: view.frame.height * 0.03).isActive = true
        
        BackButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        BackButton.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier: 0.91,constant: 0).isActive = true
        BackButton.heightAnchor.constraint(equalTo: BackButton.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        
        //signUpButton.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor,constant: 0).isActive = true
        
        view.layoutIfNeeded()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    fileprivate func setupUI (){
        portfolioLabel.font = UIFont(name: "Helvetica", size: view.frame.height * 0.05)
        messageLabel.font = UIFont(name: "Helvetica", size: view.frame.height * 0.018)
        
        sdtTextField.layer.borderWidth = 1
        sdtTextField.layer.borderColor = UIColor(white: 0.5, alpha: 0.5).cgColor
        sdtTextField.layer.cornerRadius = sdtTextField.frame.height/9
        sdtTextField.font = UIFont.systemFont(ofSize: 0.035 * view.frame.width)
        
        fullNameTextField.layer.borderWidth = 1
        fullNameTextField.layer.borderColor = UIColor(white: 0.5, alpha: 0.5).cgColor
        fullNameTextField.layer.cornerRadius = fullNameTextField.frame.height/9
        fullNameTextField.font = UIFont.systemFont(ofSize: 0.035 * view.frame.width)
        
      
        collegeTextField.layer.borderWidth = 1
        collegeTextField.layer.borderColor = UIColor(white: 0.5, alpha: 0.5).cgColor
        collegeTextField.layer.cornerRadius = collegeTextField.frame.height/9
        collegeTextField.font = UIFont.systemFont(ofSize: 0.035 * view.frame.width)
        
        usernameTextField.layer.borderWidth = 1
        usernameTextField.layer.borderColor = UIColor(white: 0.5, alpha: 0.5).cgColor
        usernameTextField.layer.cornerRadius = collegeTextField.frame.height/9
        usernameTextField.font = UIFont.systemFont(ofSize: 0.035 * view.frame.width)
        
        studentCodeTextField.layer.borderWidth = 1
        studentCodeTextField.layer.borderColor = UIColor(white: 0.5, alpha: 0.5).cgColor
        studentCodeTextField.layer.cornerRadius = collegeTextField.frame.height/9
        studentCodeTextField.font = UIFont.systemFont(ofSize: 0.035 * view.frame.width)
        
        dateOfBirthTextField.layer.borderWidth = 1
        dateOfBirthTextField.layer.borderColor = UIColor(white: 0.5, alpha: 0.5).cgColor
        dateOfBirthTextField.layer.cornerRadius = collegeTextField.frame.height/9
        dateOfBirthTextField.font = UIFont.systemFont(ofSize: 0.035 * view.frame.width)
        
        //        nameTextField.layer.borderWidth = 1
        //        nameTextField.layer.borderColor = UIColor(white: 0.5, alpha: 0.5).cgColor
        //        nameTextField.layer.cornerRadius = passwordTextField.frame.height/9
        //        nameTextField.font = UIFont.systemFont(ofSize: 0.035 * view.frame.width)
        
        BackButton.layer.cornerRadius = BackButton.frame.height/2
        BackButton.titleLabel?.font = UIFont.systemFont(ofSize: BackButton.frame.height*0.35)
        let firstColor = UIColor(hexString: "2BEEE1", withAlpha: 1)
        let secondColor  = UIColor(hexString: "08A4AE", withAlpha: 1)
        BackButton.backgroundColor = UIColor(gradientStyle: .leftToRight, withFrame: BackButton.frame, andColors: [firstColor!,secondColor!])
        
        //        let backbutton = UIButton(type: .system)
        //        backbutton.setImage(#imageLiteral(resourceName: "icons8-left-filled-50").withRenderingMode(.alwaysOriginal), for: .normal)
        //        backbutton.contentEdgeInsets = UIEdgeInsets(top: 3, left: 0, bottom: 3, right: 3)
        //        backbutton.imageView?.contentMode = .scaleAspectFit
        //        backbutton.addTarget(self, action: #selector(backToPrevious), for: .touchUpInside)
        
        //        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backbutton)
    }
    @objc func backToPrevious (){
        self.navigationController?.popViewController(animated: true)
    }
   
    override func viewDidDisappear(_ animated: Bool) {
        super .viewDidDisappear(animated)
    }
    var heightExpand  : CGFloat = 0.0
    @objc func keyboardWillShow(notification: NSNotification) {
        if keyboardHeight != nil {
            return
        }
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            
            // so increase contentView's height by keyboard height
            UIView.animate(withDuration: 0.3, animations: {
                self.heightContentViewContraint.constant += self.keyboardHeight
            })
            
            // move if keyboard hide input field
            if let acti = activeField{
                let distanceToBottom = self.scrollView.frame.size.height - (acti.frame.origin.y) - (acti.frame.size.height)
                let collapseSpace = keyboardHeight - distanceToBottom
                
                if collapseSpace < 0 {
                    // no collapse
                    return
                }
                
                // set new offset for scroll view
                UIView.animate(withDuration: 0.3, animations: {
                    // scroll to the position above keyboard 10 points
                    self.scrollView.contentOffset = CGPoint(x: self.lastOffset?.x ?? 0, y: collapseSpace + 10)
                })            }
            
        }
    }
    
   
   
    
}
