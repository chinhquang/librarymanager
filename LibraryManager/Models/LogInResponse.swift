//
//  LogInResponse.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 5/29/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import Foundation

import UIKit
import ObjectMapper
class LogInResponse: Mappable {
    var token :String?
    var code :Int?
    var msg : String?
    required init?(map: Map){
        
    }
    init() {
        
    }
    
    
    func mapping(map: Map) {
        token <- map["token"]
        code <- map["code"]
        msg <- map["msg"]
    }
}
