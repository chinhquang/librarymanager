//
//  LoginViewController.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 4/9/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

protocol LoginViewControllerDelegate: class {
    func loginSuccess(_ username : String?,_ password : String?,_ token : String?, _ message : String?)
    func loginFail(_ username : String?,_ password : String?, _ message : String?)
}

class LoginViewController: UIViewController {
    let scrollView : UIScrollView = {
        let sv = UIScrollView()
        sv.backgroundColor = .white
        
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    let loginButton : UIButton = {
        let button  = UIButton(type: .system)
        button.setTitle("LOG IN", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .white
        button.backgroundColor = .cyan
        button.addTarget(self, action: #selector(login(_:)), for: .touchUpInside)
        
        return  button
    }()
    let loginLabel : UILabel = {
        let label = UILabel()
        label.text = "LOG IN"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let messageLabel : UILabel = {
        let label = UILabel()
        label.text = "Good to see you again"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let contentView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let emailTextField : TextField = {
        let textfield  = TextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.placeholder = "Your Email ..."
        return textfield
    }()
    let passwordTextField : TextField = {
        let textfield  = TextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.placeholder = "Password ..."
        textfield.isSecureTextEntry = true
        return textfield
    }()
    weak var delegate : LoginViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(scrollView)
        
        setupLayout()
        registerKeyboardNotification()
        setupUI()
        setupBackBarButton()
    }
    func setupBackBarButton(){
        let button = UIButton(type: .system)
        button.setImage(#imageLiteral(resourceName: "circled-left--v1").withRenderingMode(.alwaysOriginal), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.addTarget(self, action: #selector(backToPrevious), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = .white
    }
    fileprivate func setupLayout(){
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        scrollView.addSubview(contentView)
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        contentView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        contentView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
        
        contentView.addSubview(loginLabel)
        loginLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        loginLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: view.frame.height * 0.17).isActive = true
        
        contentView.addSubview(messageLabel)
        messageLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        messageLabel.topAnchor.constraint(equalTo: loginLabel.bottomAnchor, constant: view.frame.height * 0.012).isActive = true
        
        contentView.addSubview(emailTextField)
        emailTextField.topAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: view.frame.height * 0.18).isActive = true
        emailTextField.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.91).isActive = true
        emailTextField.heightAnchor.constraint(equalTo: emailTextField.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        
        
        contentView.addSubview(passwordTextField)
        passwordTextField.topAnchor.constraint(equalTo: emailTextField.bottomAnchor, constant: view.frame.height * 0.03).isActive = true
        passwordTextField.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        passwordTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.91).isActive = true
        passwordTextField.heightAnchor.constraint(equalTo: emailTextField.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        
        contentView.addSubview(loginButton)
        loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor,constant: view.frame.height * 0.06).isActive = true
        loginButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        loginButton.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier: 0.91,constant: 0).isActive = true
        loginButton.heightAnchor.constraint(equalTo: loginButton.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        UI.addDoneButton(controls: [emailTextField,passwordTextField])
        view.layoutIfNeeded()
        
    }
    fileprivate func setupUI (){
        loginLabel.font = UIFont(name: "Helvetica", size: view.frame.height * 0.05)
        messageLabel.font = UIFont(name: "Helvetica", size: view.frame.height * 0.018)
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.borderColor = UIColor(white: 0.5, alpha: 0.5).cgColor
        emailTextField.layer.cornerRadius = emailTextField.frame.height/9
        emailTextField.font = UIFont.systemFont(ofSize: 0.035 * view.frame.width)
        
        passwordTextField.layer.borderWidth = 1
        passwordTextField.layer.borderColor = UIColor(white: 0.5, alpha: 0.5).cgColor
        passwordTextField.layer.cornerRadius = passwordTextField.frame.height/9
        passwordTextField.font = UIFont.systemFont(ofSize: 0.035 * view.frame.width)
        
        loginButton.layer.cornerRadius = loginButton.frame.height/2
        loginButton.titleLabel?.font = UIFont.systemFont(ofSize: loginButton.frame.height*0.35)
        let firstColor = UIColor(hexString: "2BEEE1", withAlpha: 1)
        let secondColor  = UIColor(hexString: "08A4AE", withAlpha: 1)
        loginButton.backgroundColor = UIColor(gradientStyle: .leftToRight, withFrame: loginButton.frame, andColors: [firstColor!,secondColor!])

    }
    @objc func backToPrevious (){
        print(1)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func login(_ sender : UIButton){
        if let username = emailTextField.text , let password = passwordTextField.text{
            API().login(username: username, password: password){
                response in
                if response.code == 1{
                    self.delegate?.loginSuccess(username,password,response.token, "Login successful")
                    self.dismiss(animated: true, completion: nil)
                }else {
                    self.delegate?.loginFail(username,password, response.msg)
                    self.dismiss(animated: true, completion: nil)
                }
                
            }
            
            
        }
        
       //------- this code executes login session
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super .viewDidDisappear(animated)
        deregidterKeyboardNotification()
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        scrollView.isScrollEnabled = true
        let info = notification.userInfo;
        let keyboard = (info?[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let contentInset = UIEdgeInsets(top: 0.0,left: 0.0,bottom: keyboard.height,right: 0.0);
        self.scrollView.contentInset = contentInset;
        self.scrollView.scrollIndicatorInsets = contentInset;
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        
        let info = notification.userInfo;
        let keyboard = (info?[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
        let contentInset = UIEdgeInsets(top: 0.0,left: 0.0,bottom: -keyboard.height,right: 0.0);
        self.scrollView.contentInset = contentInset;
        self.scrollView.scrollIndicatorInsets = contentInset;
    }
    func registerKeyboardNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func deregidterKeyboardNotification(){
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillShowNotification,object : nil)
        NotificationCenter.default.removeObserver(self,name: UIResponder.keyboardWillHideNotification,object : nil)
    }
    
}
