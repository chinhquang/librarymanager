//
//  LoginAndSignupViewController.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 4/9/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

import ChameleonFramework

class LoginAndSignupViewController: UIViewController{
    
    
    
    
    
    let loginButton : UIButton = {
        let button  = UIButton(type: .system)
        button.setTitle("LOG IN", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .white
        button.backgroundColor = .cyan
        button.addTarget(self, action: #selector(moveToLoginSession), for: .touchUpInside)
        return  button
    }()
    let signupButton : UIButton = {
        let button  = UIButton(type: .system)
        button.setTitle("SIGN UP", for: .normal)
        button.tintColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(moveToSignUpSession), for: .touchUpInside)
        return  button
    }()
    let dontHaveAccountLabel : UILabel = {
        let label  = UILabel()
        label.text = "Don't have an account !"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    let LogoImage : UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.image = #imageLiteral(resourceName: "applogo").withRenderingMode(.alwaysOriginal)
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(LogoImage)
        view.addSubview(loginButton)
        view.addSubview(dontHaveAccountLabel)
        view.addSubview(signupButton)
        self.navigationController?.removeBorder()
        setupLayout()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = .white
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    private func setupLayout () {
        
        LogoImage.topAnchor.constraint(equalTo: view.topAnchor, constant: view.frame.height * 0.15 ).isActive = true
        LogoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        LogoImage.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6).isActive = true
        
        loginButton.topAnchor.constraint(equalTo: LogoImage.bottomAnchor,constant: view.frame.height * 0.075).isActive = true
        loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginButton.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier: 0.91,constant: 0).isActive = true
        loginButton.heightAnchor.constraint(equalTo: loginButton.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        
        dontHaveAccountLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        dontHaveAccountLabel.topAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: 0.07 * view.frame.height).isActive = true
        
        signupButton.topAnchor.constraint(equalTo: dontHaveAccountLabel.bottomAnchor,constant: view.frame.height * 0.04).isActive = true
        signupButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        signupButton.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier: 0.91,constant: 0).isActive = true
        signupButton.heightAnchor.constraint(equalTo: signupButton.widthAnchor,multiplier: 0.14,constant: 0).isActive = true
        view.layoutIfNeeded()
        
        setupUI()
        
        
    }
    
    private func setupUI (){
        //login button
        loginButton.layer.cornerRadius = loginButton.frame.height/2
        loginButton.titleLabel?.font = UIFont.systemFont(ofSize: loginButton.frame.height*0.35)
        let firstColor = UIColor(hexString: "2BEEE1", withAlpha: 1)
        let secondColor  = UIColor(hexString: "08A4AE", withAlpha: 1)
        loginButton.backgroundColor = UIColor(gradientStyle: .leftToRight, withFrame: loginButton.frame, andColors: [firstColor!,secondColor!])
        //message
        dontHaveAccountLabel.font = UIFont.systemFont(ofSize: loginButton.frame.height*0.35)
        //signup button
        signupButton.layer.cornerRadius = signupButton.frame.height/2
        signupButton.layer.borderWidth = 1.5
        signupButton.layer.borderColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        signupButton.titleLabel?.font = UIFont.systemFont(ofSize: signupButton.frame.height*0.35)
        
        navigationItem.setHidesBackButton(true, animated: false)
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    @objc func moveToLoginSession(){
        let loginVC = LoginViewController()
        loginVC.delegate = self
        present(loginVC, animated: true, completion: nil)
        
    }
    @objc func moveToSignUpSession(){
        let signupVC = SignUpViewController()
        signupVC.delegate = self
        present(signupVC, animated: true, completion: nil)
    }
    
}

extension LoginAndSignupViewController : LoginViewControllerDelegate{
    func loginSuccess(_ username: String?, _ password: String?, _ token: String?, _ message: String?) {
        guard let token = token else {
            return
        }
        API.token = token
        DispatchQueue.main.async {
            self.present(TabBarController(), animated: true, completion: nil)
        }
    }
    
//    func loginSuccess(_ username: String?, _ password: String?, _ token: String?, _ message: String?) {
//        DispatchQueue.main.async {
//            self.present(TabBarController(), animated: true, completion: nil)
//        }
//
//
//    }
    func loginFail(_ username: String?, _ password: String?, _ message: String?) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Failed", message: message, preferredStyle: .actionSheet)
            
            
            
            alert.addAction(UIAlertAction(title: "OK", style: .default) { [unowned self]  result in
                
                
                
            })
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
}
extension LoginAndSignupViewController : SignUpViewControllerDelegate{
    func signupSuccess(_ userprofile: UserProfile?, _ message: String?) {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Sucess", message: message, preferredStyle: .actionSheet)
            
            
            
            alert.addAction(UIAlertAction(title: "OK", style: .default) { [unowned self]  result in
                
                
                
            })
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func signupFail(_ userprofile: UserProfile?, _ message: String?) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Failed", message: message, preferredStyle: .actionSheet)
          
            
            
            alert.addAction(UIAlertAction(title: "OK", style: .default) { [unowned self]  result in
                
                
                
            })
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
}
