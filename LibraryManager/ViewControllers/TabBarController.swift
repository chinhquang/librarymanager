//
//  TabBarController.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 5/30/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        let home = HomePageCollectionViewController(collectionViewLayout: StretchyHeaderLayout())
        home.tabBarItem = UITabBarItem(title: "HomePage", image: #imageLiteral(resourceName: "icons8-home-page-50"), tag: 0)
        let nav1 = UINavigationController (rootViewController: home)
        let search = SearchResultTableViewController()
        
        search.tabBarItem = UITabBarItem(title: "Search", image: #imageLiteral(resourceName: "icons8-search-48"), tag: 1)
        let nav2 = UINavigationController(rootViewController: search)
        
        let library = LibraryViewController()
        library.tabBarItem = UITabBarItem(title: "Libary", image: #imageLiteral(resourceName: "icons8-book-50"), tag: 2)
        let nav3 = UINavigationController(rootViewController: library)
        // set up tabbar
        
        let setting = SettingTableViewController()
        setting.tabBarItem = UITabBarItem(title: "Setting", image: #imageLiteral(resourceName: "icons8-settings-50"), tag: 3)
        let nav4 = UINavigationController(rootViewController: setting)
        // set up tabbar
        
        let tabBarList = [nav1,nav2, nav3, nav4]
        viewControllers = tabBarList
        // Do any additional setup after loading the view.
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
