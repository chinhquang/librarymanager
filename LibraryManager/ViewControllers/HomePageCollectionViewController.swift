import UIKit

import SDWebImage


class HomePageCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, BookCellListDelegate {
    func bookList(didSelectAt indexPath: IndexPath) {
        print("Cac")
    }
    
    
    
    
    fileprivate let cellId = "cellId"
    fileprivate let cellId2 = "cellId2"
    fileprivate let headerId = "headerId"
    fileprivate let padding: CGFloat = 16
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.removeBorder()
        setupCollectionViewLayout()
        setupCollectionView()
    }
    
    fileprivate func setupCollectionViewLayout() {
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionInset = .init(top: padding, left: padding, bottom: padding, right: padding)
        }
    }
    
    
    
    fileprivate func setupCollectionView() {
        collectionView.backgroundColor = .white
        collectionView.contentInsetAdjustmentBehavior = .never
        collectionView.alwaysBounceVertical = true
        collectionView.register(BookCellList.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(AuthorCellList.self, forCellWithReuseIdentifier: cellId2)
        collectionView.register(HeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerId)
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let contentOffsetY = scrollView.contentOffset.y
        print(contentOffsetY)
        
        if contentOffsetY > 0 {
            headerView?.animator.fractionComplete = 0
            return
        }
        
        headerView?.animator.fractionComplete = abs(contentOffsetY) / 100
        
        
    }
    
    var headerView: HeaderView?
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as? HeaderView
            return headerView!
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .init(width: view.frame.width, height: view.frame.height / 3 )
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
        
    }
    

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionView{
            if indexPath.item == 0 {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as? BookCellList{
                    cell.delegate = self
                    cell.backgroundColor = .white
                    cell.label.text = "Popular books"
                    
                    return cell
                }
            }
            else if indexPath.item == 1 {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId2, for: indexPath) as? AuthorCellList{
                    cell.backgroundColor = .white
                    cell.label.text = "Popular authors"
                    
                    return cell
                }
            }
        }
       

        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width - 2 * padding, height: view.frame.height * 2 / 7.5)
    }
    
    
}
