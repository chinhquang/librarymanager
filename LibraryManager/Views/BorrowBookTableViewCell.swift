//
//  BorrowBookTableViewCell.swift
//  LibraryManager
//
//  Created by Chính Trình Quang on 6/11/19.
//  Copyright © 2019 Chính Trình Quang. All rights reserved.
//

import UIKit
import  Cosmos
class BorrowBookCell: UITableViewCell {
    let bookImg : UIImageView = {
        let img = UIImageView()
        
        img.translatesAutoresizingMaskIntoConstraints = false
        img.image = #imageLiteral(resourceName: "placeholder-book-cover-default")
        img.contentMode = .scaleAspectFit
        img.layer.borderColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        img.layer.borderWidth = 1.2
        img.layer.cornerRadius = 5
        return img
        
    }()
    var book : Book? {
        didSet{
            self.bookImg.sd_setImage(with: URL(string: book!.image!), placeholderImage: #imageLiteral(resourceName: "placeholder-book-cover-default"))
            self.authorLabel.text = self.book?.author
            self.nameLabel.text = self.book?.name
            
        }
    }
    var returndate : Int? {
        didSet{
            let timestamp = self.returndate!
            self.returnDateLabel.text = "Return date : "  + timestamp.toDateTime().toString()
        }
    }
    let nameLabel : UILabel = {
        let lb = UILabel()
        lb.text = "Phép lạ"
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.numberOfLines = 0
        lb.textColor = #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)
        return lb
    }()
    let authorLabel : UILabel = {
        let lb = UILabel()
        lb.text = "Tạ Duy Anh"
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.numberOfLines = 0
        lb.textColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        return lb
    }()
    let returnDateLabel : UILabel = {
        let lb = UILabel()
        lb.text = "1998-18-12"
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.numberOfLines = 0
        return lb
    }()
    let rating : CosmosView = {
        let rt = CosmosView()
        rt.translatesAutoresizingMaskIntoConstraints = false
        return rt
    }()
    func setUpView() -> Void {
        self.addSubview(bookImg)
        bookImg.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 12).isActive = true
        bookImg.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: frame.width * 0.06).isActive = true
        bookImg.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.29).isActive = true
        bookImg.heightAnchor.constraint(equalTo: bookImg.widthAnchor, multiplier: 1.5).isActive = true
        
        
        self.addSubview(nameLabel)
        
        nameLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 12).isActive = true
        nameLabel.leadingAnchor.constraint(equalTo: bookImg.trailingAnchor, constant: frame.width * 0.06).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -self.frame.width * 0.04).isActive = true
        
        
        nameLabel.font = UIFont(name: "Helvetica", size: self.frame.width * 0.055)
        
        self.addSubview(authorLabel)
        
        authorLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 6).isActive = true
        authorLabel.leadingAnchor.constraint(equalTo: bookImg.trailingAnchor, constant: self.frame.width * 0.06).isActive = true
        authorLabel.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -self.frame.width * 0.04).isActive = true
        
        
        authorLabel.font = UIFont(name: "Helvetica", size: self.frame.width * 0.04)
        
        self.addSubview(returnDateLabel)
        
        returnDateLabel.topAnchor.constraint(equalTo: authorLabel.bottomAnchor, constant: 6).isActive = true
        returnDateLabel.leadingAnchor.constraint(equalTo: bookImg.trailingAnchor, constant: self.frame.width * 0.06).isActive = true
        returnDateLabel.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -self.frame.width * 0.04).isActive = true
        
        
        returnDateLabel.font = UIFont(name: "Helvetica", size: self.frame.width * 0.04)
        self.addSubview(rating)
        
        rating.topAnchor.constraint(equalTo: returnDateLabel.bottomAnchor, constant: 12).isActive = true
        rating.leadingAnchor.constraint(equalTo: bookImg.trailingAnchor, constant: self.frame.width * 0.06).isActive = true
        rating.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant:  -self.frame.width * 0.04).isActive = true
        rating.heightAnchor.constraint(equalTo: rating.widthAnchor, multiplier: 0.2)
        rating.rating = 3.5
        rating.settings.starSize = Double(rating.frame.height - 3)
        rating.settings.starMargin = 0
        rating.text = "Rating"
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

}
